﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Web : MonoBehaviour
{
    [System.Serializable]
    public struct EstructuraDatosWeb
    {
        [System.Serializable]
        public struct registro
        {
            public string nombre;
            public int puntaje;
        }
        public List<registro> registros;
        public string fechaHora;        
        //public int nivel;
        
    }
    
    public EstructuraDatosWeb datos;
    public Transform tabla;
    public Transform nuevo;
    public GameObject plantillaDeRegistros;
    int cantidadDeRegistros = 5;
    public int miPuntaje;
    public InputField miNombre;

    [ContextMenu("Leer Simple")]
    public void LeerSimple()
    {
        //Debug.Log("Hola");
        StartCoroutine(CorrutinaLeerSimple());
    }

    IEnumerator CorrutinaLeerSimple()
    {
        //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
        UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/pruebaTutorial.txt");
        yield return web.SendWebRequest();
        //Esperamos a que vuelva...
        //volvió
        if (!web.isNetworkError &&  !web.isHttpError)
        {
            //todo ok
            Debug.Log(web.downloadHandler.text);
        }else
        {
            Debug.LogWarning("Hubo un problema");
        }
    }

     [ContextMenu("Escribir Simple")]
    public void EscribirSimple()
    {
        //Debug.Log("Hola");
        StartCoroutine(CorrutinaEscribirSimple());
    }

    IEnumerator CorrutinaEscribirSimple()
    {
        WWWForm form = new WWWForm();
        form.AddField("archivo", "pruebaTutorial.txt");
        form.AddField("texto", "Hola. Primera prueba completa desde Unity. Segunda escritura");



        //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
        UnityWebRequest web = UnityWebRequest.Post(
            "https://pipasjourney.com/compartido/escribir.php",
            form);
        yield return web.SendWebRequest();
        //Esperamos a que vuelva...
        //volvió
        if (!web.isNetworkError &&  !web.isHttpError)
        {
            //todo ok
            Debug.Log(web.downloadHandler.text);
        }else
        {
            Debug.LogWarning("Hubo un problema");
        }
    }

    // [ContextMenu("Escribir Varios datos sin JSON")]
    // public void EscribirVariosSinJson()
    // {
    //     //Debug.Log("Hola");
    //     StartCoroutine(CorrutinaEscribirVariosSinJson());
    // }

    // IEnumerator CorrutinaEscribirVariosSinJson()
    // {
    //     WWWForm form = new WWWForm();
    //     form.AddField("archivo", "pruebaVariosSinJson.txt");
    //     form.AddField("texto", nombre + "♣" +puntaje.ToString()+ "♣" + nivel.ToString());



    //     //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
    //     UnityWebRequest web = UnityWebRequest.Post(
    //         "https://pipasjourney.com/compartido/escribir.php",
    //         form);
    //     yield return web.SendWebRequest();
    //     //Esperamos a que vuelva...
    //     //volvió
    //     if (!web.isNetworkError &&  !web.isHttpError)
    //     {
    //         //todo ok
    //         Debug.Log(web.downloadHandler.text);
    //     }else
    //     {
    //         Debug.LogWarning("Hubo un problema");
    //     }
    // }

    
    // [ContextMenu("Leer Varios datos sin Json")]
    // public void LeerVariosSinJson()
    // {
    //     //Debug.Log("Hola");
    //     StartCoroutine(CorrutinaLeerVariosSinJson());
    // }

    // IEnumerator CorrutinaLeerVariosSinJson()
    // {
    //     //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
    //     UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/pruebaVariosSinJson.txt");
    //     yield return web.SendWebRequest();
    //     //Esperamos a que vuelva...
    //     //volvió
    //     if (!web.isNetworkError &&  !web.isHttpError)
    //     {
    //         //todo ok
    //         string textoOriginal = web.downloadHandler.text;
    //         string[] partes = textoOriginal.Split('♣');
    //         nombre = partes[0];
    //         puntaje = int.Parse(partes[1]);
    //         nivel = int.Parse(partes[2]);
    //         Debug.Log(web.downloadHandler.text);
    //     }else
    //     {
    //         Debug.LogWarning("Hubo un problema");
    //     }
    // }

    [ContextMenu("Leer")]
    public void Leer(System.Action accionAlTerminar)
    {
        //Debug.Log("Hola");
        StartCoroutine(CorrutinaLeer(accionAlTerminar));
    }

    IEnumerator CorrutinaLeer(System.Action accionAlTerminar)
    {
        //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
        //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/pruebaJson.txt");
        UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/tablaHiscores.txt");
        yield return web.SendWebRequest();
        //Esperamos a que vuelva...
        //volvió
        if (!web.isNetworkError &&  !web.isHttpError)
        {
            //todo ok
            Debug.Log(web.downloadHandler.text);
            datos =JsonUtility.FromJson<EstructuraDatosWeb>( web.downloadHandler.text);
            accionAlTerminar();
        }else
        {
            Debug.LogWarning("Hubo un problema");
        }
    }

     [ContextMenu("Escribir")]
    public void Escribir()
    {
        //Debug.Log("Hola");
        StartCoroutine(CorrutinaEscribir());
    }

    IEnumerator CorrutinaEscribir()
    {
        WWWForm form = new WWWForm();
        //form.AddField("archivo", "pruebaJson.txt");
        form.AddField("archivo", "tablaHiscores.txt");
        form.AddField("texto", JsonUtility.ToJson(datos));



        //UnityWebRequest web = UnityWebRequest.Get("https://pipasjourney.com/compartido/hola.txt");
        UnityWebRequest web = UnityWebRequest.Post(
            "https://pipasjourney.com/compartido/escribir.php",
            form);
        yield return web.SendWebRequest();
        //Esperamos a que vuelva...
        //volvió
        if (!web.isNetworkError &&  !web.isHttpError)
        {
            //todo ok
            Debug.Log(web.downloadHandler.text);
        }else
        {
            Debug.LogWarning("Hubo un problema");
        }
    }

[ContextMenu("Crear tabla")]
    void CrearTabla()
    {
        for (int i = 0; i < cantidadDeRegistros; i++)
        {
            GameObject inst = Instantiate(plantillaDeRegistros, tabla);
            inst.GetComponent<RectTransform>().anchoredPosition = new Vector2 (0, i * -50);
            inst.name = i.ToString();
        }
    }

    [ContextMenu("Pasar datos a tabla")]
    void PasarDatosATabla()
    {
        for (int i = 0; i < cantidadDeRegistros; i++)
        {
            tabla.GetChild(i).GetChild(0).GetComponent<Text>().text = datos.registros[i].nombre;
            tabla.GetChild(i).GetChild(1).GetComponent<Text>().text = datos.registros[i].puntaje.ToString();
        }
    }

    [ContextMenu("Chequear Si Corresponde")]
    void ChequearSiCorrespondeNuevoHiscore()
    {
        if(miPuntaje > datos.registros[cantidadDeRegistros - 1].puntaje)
        {
            //Si, corresponde
            tabla.gameObject.SetActive(false);
            nuevo.gameObject.SetActive(true);
        }
        else
        {
            tabla.gameObject.SetActive(true);
            nuevo.gameObject.SetActive(false);
        }
    }

    [ContextMenu("Insertar Registro")]
    void InsertarNuevoRegistro()
    {
        //Saber en que posicion tiene que insertar
        for (int i = 0; i < cantidadDeRegistros; i++)
        {
            if (miPuntaje > datos.registros[i].puntaje)
            {
                //inserto
                datos.registros.Insert(i, new EstructuraDatosWeb.registro()
                {
                    nombre = miNombre.text,
                    puntaje = miPuntaje
                });

                break;//para que salga del for
            }
        }
    }

    void Start()
    {
        Leer(CrearTablaPasarDatosYChequear);
    }

    void CrearTablaPasarDatosYChequear()
    {
        CrearTabla();
        PasarDatosATabla();
        ChequearSiCorrespondeNuevoHiscore();
    }

    public void InputTermino()
    {
        nuevo.gameObject.SetActive(false);
        tabla.gameObject.SetActive(true);
        Leer(InsertarYEscribir);
    }

    void InsertarYEscribir()
    {
        InsertarNuevoRegistro();
        Escribir();
        PasarDatosATabla();
    }
}